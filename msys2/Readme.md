# MSYS 2 with Git

- Install msys2 from https://www.msys2.org/
- From https://github.com/git-for-windows/git/wiki/Install-inside-MSYS2-proper
- Edit `c:\msys\etc\pacman.conf` and add the following *before* `[mingw32]` (the `-mingw32` Version is otherwise added automatically and requires another update)
```ini
[git-for-windows]
Server = https://wingit.blob.core.windows.net/x86-64

[git-for-windows-mingw32]
Server = https://wingit.blob.core.windows.net/i686
```

- System update (close window afterwards)
```bash
curl -L https://raw.githubusercontent.com/git-for-windows/build-extra/HEAD/git-for-windows-keyring/git-for-windows.gpg |
pacman-key --add - &&
pacman-key --lsign-key E8325679DFFF09668AD8D7B67115A57376871B1C &&
pacman-key --lsign-key 3B6D86A1BA7701CD0F23AED888138B9E1A9F3986
pacman -Syyuu
```
- Open new terminal
```bash
pacman -Suu
pacman -S mingw-w64-x86_64-git mingw-w64-x86_64-curl mingw-w64-x86_64-git-lfs
pacman -S openssh
pacman -S zsh rsync zip unzip p7zip atool
pacman -S mingw-w64-x86_64-ruby mingw-w64-x86_64-python
pacman -S mingw-w64-x86_64-clang mingw-w64-x86_64-gcc
```
