# Windows Configuration Scripts
Small collection of scripts to customize Windows 10 installations. Probably not usable as is but might be useful as a toolbox to pick and choose. If you want to take one thing away, have a look at <https://github.com/Disassembler0/Win10-Initial-Setup-Script>.

## Overview
* initial-setup: download script and preset for [Win10-Initial-Setup-Script](https://github.com/Disassembler0/Win10-Initial-Setup-Script)
* additional-setup: a few other scripts that might be useful for setup
* choco: install the [Chocolatey](https://chocolatey.org/) package manager and scripts to install common programs
* cygwin: install [Cygwin](https://www.cygwin.com/) 32 bits and scripts to install common packages

## License
[The Unlicense](LICENSE)
