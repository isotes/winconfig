cinst -y temurin17 --override --installargs "ADDLOCAL=FeatureMain,FeatureEnvironment,FeatureJavaHome INSTALLDIR=""%ProgramFiles%\JDK11Temurin"" /quiet"

cinst -y temurin8 --override --installargs "ADDLOCAL=FeatureMain INSTALLDIR=""%ProgramFiles%\JDK8Temurin"" /quiet"

cinst -y gradle maven

@pause
