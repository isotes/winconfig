$base = "https://raw.githubusercontent.com/Disassembler0/Win10-Initial-Setup-Script/master/"
$files = @("Win10.ps1", "Win10.psm1", "Default.preset")

Foreach ($f in $files) {
	Write-Output $base$f
	Invoke-WebRequest -Uri $base$f -OutFile $f
}

Read-Host -Prompt "Press Enter to continue"
