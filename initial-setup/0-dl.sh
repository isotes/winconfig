#!/bin/bash
set -euo pipefail

cd "${0%/*}"

for f in Win10.ps1 Win10.psm1 Default.preset; do
	curl --silent --show-error --fail "https://raw.githubusercontent.com/Disassembler0/Win10-Initial-Setup-Script/master/$f" > $f
done
