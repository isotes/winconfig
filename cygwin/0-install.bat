@echo off
echo "Important:"
echo "This installs the 32 Bit version of cygwin, but afterwards"
echo "C:\cygwin\cygwinsetup.exe is nevertheless the 64 Bit version so"
echo "subsequent installs of packages will mess things up"
echo "-> Overwrite the file with"
echo "C:\ProgramData\chocolatey\lib\Cygwin\tools\setup-x86.exe"
echo "(which we try, but have not tested extensively"

choco install --forcex86 --yes cygwin --params='"/InstallDir:c:\cygwin /Site:http://linux.rz.ruhr-uni-bochum.de/download/cygwin/"'

copy "C:\ProgramData\chocolatey\lib\Cygwin\tools\setup-x86.exe" "C:\cygwin\cygwinsetup.exe"

@pause
